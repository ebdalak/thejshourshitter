import React from 'react';
import axios from 'axios';

const ClockifyData = async (props) => {
    const listItems = props.endpoints;
    const rv = await getData(listItems)
    console.log({rv})
    return rv
}

export default ClockifyData;

const getData = async (listItems) => {
    let rv = {}
    rv['users'] = await query("users")
    rv['projects'] = await query("projects")
    rv['clients'] = await query("clients")
    
    return (rv);
}

const query = (item) => {
    console.log(item)
    return axios({
        method: 'get',
        method: 'get',
        url: 'https://api.clockify.me/api/v1/workspaces/5c063627b0798723e0fa22f4/' + item,
        responseType: 'json',
        headers: { 'X-Api-Key': 'XA0GCbB5h2QwUI1L' }
    })
}

const error = function (error) {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
    console.log(error.config);
}
