import React, { useState, Component} from 'react';
import ClockifyData from './api/GetClockifyData';
import MenuItem from './bl/GenerateDropDowns';
import DropDown from './components/DropDown';
import DatePicker from './components/DatePicker';

var endpoints = ["users", "clients", "projects"];

export default class App extends Component {
    
    state = {
        data: {}
    }
    
    constructor() {
        super()
        MenuItem({endpoints}).then(rv => {this.setState({data:rv})})
    }
    render() {
        return (<div>
                 < DropDown endpoints = { this.state.data } />
                < DatePicker identifier = "Start Date" / >
                < DatePicker identifier = "End Date" / >
                </div>)
    }
};
