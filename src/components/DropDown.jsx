import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


/*
const DropDown = (props) => {
    const classes = useStyles();
    const [selectedValue, setselectedValue] = React.useState('0');
    const [selectedName, setselectedName] = React.useState('1');

    const handleChange = event => {
        this.props.value({value : event.target.value});
        this.props.name({name : event.target.value});8?
      /*  setselectedValue({value : event.target.value});
        setselectedName({name : event.target.name});*/
   /* };

    console.log(props.endpoint)
    return (
        <div >
            <FormControl key={Math.random()} className={classes.formControl}>
                <InputLabel key={Math.random()} id={props.title}>{props.title}</InputLabel>
                
                <Select
                    key={Math.random()}
                    labelId={props.title}
                    id={props.title}
                    value={selectedValue}
                    name={props.title}
                    onChange={handleChange}
                >
                    {
                        props.endpoint.data.map(obj =>
                            <MenuItem key={obj.id} multiple={true} value={[obj.name, obj.id]} >{obj.name}</MenuItem>
                        )
                    }
                </Select>
            </FormControl>
        </div>

    )
}

export default DropDown;
*/


export default class DropDown extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
    };
  
    classes = props => makeStyles(theme => ({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
    }));

//  classes = useStyles();
 
 handleChange = event => {
        this.props.onDropDownChange({value : event.target.value});
        // this.props.onDropDownChange({value : event.target.value, name : event.target.name});

        // this.props.onDropDownChangen.name({name : event.target.name});
    };

  
    render() {
      const name = this.props.name;
      const value = this.props.value;
return (
        <div >
            <FormControl key={Math.random()} className={this.classes.formControl}>
                <InputLabel key={Math.random()} id={this.props.title}>{this.props.title}</InputLabel>
                
                <Select
                    key={Math.random()}
                    labelId={this.props.title}
                    id={this.props.title}
                    value={value}
                    name={name}
                    onChange={this.handleChange}
                >
                    {
                        this.props.endpoint.data.map(obj =>
                            <MenuItem key={obj.id} multiple={true} value={[obj.name, obj.id]} >{obj.name}</MenuItem>
                        )
                    }
                </Select>
            </FormControl>
        </div>

    )
    }
  }

  /*
  class TemperatureInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onTemperatureChange(e.target.value);
  }

  render() {
    const temperature = this.props.temperature;
    const scale = this.props.scale;
    return (
      <fieldset>
        <legend>Enter temperature in {scaleNames[scale]}:</legend>
        <input value={temperature}
               onChange={this.handleChange} />
      </fieldset>
    );
  }
}
*/