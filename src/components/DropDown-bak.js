import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));
const DropDown = (props) => {
    const classes = useStyles();
    const [selectedValue, setselectedValue] = React.useState('');

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);

    const handleChange = event => {
        setselectedValue(event.target.value);
    };

    const listItems = Object.keys(props.endpoints).map((item, index) => {
        console.log(props.endpoints[item].data)

        return (
            <div >
                <FormControl key={Math.random()} className={classes.formControl}>
                    <InputLabel key={Math.random()} id={item}>{item}</InputLabel>
                    <Select
                        key={Math.random()}
                        labelId={item}
                        id={item}
                        value={selectedValue}
                        onChange={handleChange}
                    >
                        {
                            props.endpoints[item].data.map(obj =>
                                <MenuItem key={Math.random()} value={obj.id}>{obj.name}</MenuItem>
                            )
                        }
                    </Select>
                </FormControl>
            </div>

        )
    });

    return (listItems);
}

export default DropDown;