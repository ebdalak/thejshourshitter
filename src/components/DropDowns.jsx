import React, { Component } from 'react';
import DropDown from './DropDown';
import ClockifyData from '../api/GetClockifyData';
//import { isThisSecond } from 'date-fns';

var endpoints = ["users", "clients", "projects"];

export default class DropDowns extends Component {
    
    state = {
        data: undefined 
        // [name]: value
    }

    constructor() {
        super()
        ClockifyData({ endpoints }).then(rv => { this.setState({ data: rv }) });
        this.handleUsersChange = this.handleUsersChange.bind(this);
        this.handleProjectsChange = this.handleProjectsChange.bind(this);
        this.handleClientsChange = this.handleClientsChange.bind(this);
    }

        handleUsersChange(value) {
        this.setState({user: value});
      }
    
      handleProjectsChange(value) {
        this.setState({project: value});
      }

      handleClientsChange(value) {
        this.setState({client: value});
      }

    render() {
        const name = this.state.name;
        const value = this.state.value;
        return this.state.data !== undefined ? <div>
            < DropDown title='Users' name={name} value={value} onDropDownChange={this.handleUsersChange} endpoint={this.state.data.users} />
            < DropDown title='Projects' name={name} value={value} onDropDownChange={this.handleProjectsChange} endpoint={this.state.data.projects} />
            < DropDown title='Clients' name={name} value={value} onDropDownChange={this.handleClientsChange} endpoint={this.state.data.clients} />
        </div> : <div />
    }
};




/*
class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.handleCelsiusChange = this.handleCelsiusChange.bind(this);
    this.handleFahrenheitChange = this.handleFahrenheitChange.bind(this);
    this.state = {temperature: '', scale: 'c'};
  }

  handleCelsiusChange(temperature) {
    this.setState({scale: 'c', temperature});
  }

  handleFahrenheitChange(temperature) {
    this.setState({scale: 'f', temperature});
  }

  render() {
    const scale = this.state.scale;
    const temperature = this.state.temperature;
    const celsius = scale === 'f' ? tryConvert(temperature, toCelsius) : temperature;
    const fahrenheit = scale === 'c' ? tryConvert(temperature, toFahrenheit) : temperature;

    return (
      <div>
        <TemperatureInput
          scale="c"
          temperature={celsius}
          onTemperatureChange={this.handleCelsiusChange} />
        <TemperatureInput
          scale="f"
          temperature={fahrenheit}
          onTemperatureChange={this.handleFahrenheitChange} />
        <BoilingVerdict
          celsius={parseFloat(celsius)} />
      </div>
    );
  }
}

ReactDOM.render(
  <Calculator />,
  document.getElementById('root')
  */